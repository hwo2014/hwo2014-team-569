import json
import socket
import sys
import math


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.carColor = None
        self.pieceArray = []
        self.pings = 0
        self.mph = 0
        self.lastDist = 0
        self.curPiece = None
        self.turboAvail = False
        self.myPiece = None
        self.curThrottle = 0.5
        self.turnSpeed = 0.0
        self.rapidSlow = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name" :self.name,
                                 "key"  : self.key})
#{#"botId":{"name": self.name,
                                 #"key": self.key}})
				 #"trackName":"usa",
				# "carCount":1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch(self, direction):
	self.msg("switchLane", direction)
    
    def turbo(self):
        if self.turboAvail == True:
            self.turboAvail = False
            self.msg("turbo", "SPEEEEEEEEEEEEEEEEEED")

    def turboAvailable(self, data):
        self.rapidSlow = True
        self.turboAvail = True
    
    def speed(self, curSpeed, targetSpeed):
        if targetSpeed == "turbo":
            return 1
        elif curSpeed < targetSpeed:
            if self.curThrottle+0.1 <= 1:
                return self.curThrottle+0.1
            else:
                return 1
        elif curSpeed > targetSpeed:
            if self.rapidSlow:
                if self.curThrottle-0.3 >= 0.1:
                    return self.curThrottle-0.3
                else:
                    return 0.1
            else:
                if self.curThrottle-0.1 >= 0.1:
                    return self.curThrottle-0.1
                else:
                    return 0.1
        else:
            return self.curThrottle
	    
    def drift(self, angle):
        #self.throttle(((100-math.fabs(self.pieceArray[self.curPiece]['angle']))*0.01)+0.15)
        self.curThrottle = self.speed(self.mph, 5.5) #55
        self.throttle(self.curThrottle)
        if self.leave_turn(self.curPiece):
            self.curThrottle = self.speed(self.mph, "turbo")
            self.throttle(self.curThrottle)
            self.rapidSlow = False
        '''
        if angle >= 20:
            #self.curThrottle = self.speed(self.mph, self.mph-0.05)
            self.curThrottle = self.speed(self.mph, 5)
            self.throttle(self.curThrottle)
        elif angle < 20:
            self.curThrottle = self.speed(self.mph, 7.5) #7.5
            self.throttle(self.curThrottle)
        else:
            self.curThrottle = self.speed(self.mph, self.mph)
            self.throttle(self.curThrottle)
        '''
        
	    

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def next_piece(self, index):
        if(index+1 < len(self.pieceArray)):
            return self.pieceArray[index+1]
        else:
            return self.pieceArray[index]
	
    def leave_turn(self, index):
        if(index+1 < len(self.pieceArray)):
            for x in range(1, 1):  #was 3
                if self.pieceArray[index + x]['type'] == 'straight':
		            return True
            return False
        else:
            return True

    def approach_turn(self,index):
        if (index + 3 < len(self.pieceArray)):
            for x in range(1,3):
                if self.pieceArray[index + x]['type'] == 'turn':
                    return True
            return False
        else:
            return False

    def drag_strip(self,index):
        if (index+5 < len(self.pieceArray)):
            for x in range(1,5):
                if self.pieceArray[index + x]['type'] == 'straight':
                    continue
                else:
                    return False
            return True
        else:
            return False

    def approach_switch(self,index):
        if (index + 2 < len(self.pieceArray)):
            for x in range(1,2):
                if 'switch' in self.pieceArray[index + x]:
                    if self.pieceArray[index+x+1]['type'] == "turn":
                        return index + x
                    else:
                        return False
            return False
        else:
            return False

    def pick_lane(self,index):
        if (self.pieceArray[index+1]['angle'] > 0):
            self.switch("Right")
            self.curThrottle = self.speed(self.mph, 6)
            self.throttle(self.curThrottle)
        elif(self.pieceArray[index+1]['angle'] < 0):
            self.switch("Left")
            self.curThrottle = self.speed(self.mph, 6)
            self.throttle(self.curThrottle)
        else:
            if self.approach_turn(self.curPiece):
                self.curThrottle = self.speed(self.mph, 6)
                self.throttle(self.curThrottle)
            elif self.leave_turn(self.curPiece):
                self.curThrottle = self.speed(self.mph, 10)
                self.throttle(self.curThrottle)
            elif self.drag_strip(curPiece):
                self.turbo()
                self.curThrottle = self.speed(self.mph, "turbo")
                self.throttle(self.curThrottle) 
            else:
                self.curThrottle = self.speed(self.mph, 6)
                self.throttle(self.curThrottle)

    def on_car_positions(self, data):
        for x in range(len(data)):
            if data[x]['id']['color'] == self.carColor:
                self.myPiece = x
        inDist = data[self.myPiece]["piecePosition"]["inPieceDistance"]
        if self.curPiece == None:
            self.curPiece = data[self.myPiece]["piecePosition"]["pieceIndex"]
        elif self.curPiece != data[self.myPiece]["piecePosition"]["pieceIndex"]:
            self.curPiece = data[self.myPiece]["piecePosition"]["pieceIndex"]
            if self.pieceArray[self.curPiece]['type'] == 'straight':
                if self.pieceArray[self.curPiece-1]['type'] == 'straight':
                    self.mph = ((self.pieceArray[self.curPiece-1]['length']-self.lastDist)+inDist)
                else:
                    self.mph = math.fabs(((2*math.pi*self.pieceArray[self.curPiece-1]['radius']*self.pieceArray[self.curPiece-1]['angle'])/360)-self.lastDist)+inDist
                self.lastDist = inDist
            else:
                if self.pieceArray[self.curPiece-1]['type'] == 'straight':
                    self.mph = ((self.pieceArray[self.curPiece-1]['length']-self.lastDist)+inDist)
                else:
                    self.mph = math.fabs(((2*math.pi*self.pieceArray[self.curPiece-1]['radius']*self.pieceArray[self.curPiece-1]['angle'])/360)-self.lastDist)+inDist
                self.lastDist = inDist
            if self.mph > 0:
                print("%.2f"%self.mph)
        else:
            self.curPiece = data[self.myPiece]['piecePosition']['pieceIndex']
            self.mph = inDist-self.lastDist
            self.lastDist = inDist
            #if self.pieceArray[self.curPiece]['type'] == 'straight':
            #    self.mph = self.inDist/self.pings
            #    self.pings = 0
            #else:
            #    self.mph = math.fabs(((2*math.pi*self.pieceArray[self.curPiece]['radius']*self.pieceArray[self.curPiece]['angle'])/360)/self.pings)
            #    self.pings = 0
            if self.mph > 0:
                print("%.2f"%self.mph)
        turnIndex = self.approach_switch(self.curPiece)
        if(turnIndex != False):
            self.turnSpeed = self.mph
            self.pick_lane(turnIndex)
        elif (self.pieceArray[self.curPiece]["type"] == "straight"):
            self.turnSpeed = self.mph
            if self.approach_turn(self.curPiece):
                self.curThrottle = self.speed(self.mph, 5.5)
                self.throttle(self.curThrottle)
            elif self.drag_strip(self.curPiece):
                self.turbo()
                self.curThrottle = self.speed(self.mph, "turbo")
                self.throttle(self.curThrottle)
            elif (self.next_piece(self.curPiece)['type'] == 'straight'):
                self.curThrottle = self.speed(self.mph, 10)
                self.throttle(self.curThrottle)
            else:
                self.curThrottle = self.speed(self.mph, 10)
                self.throttle(self.curThrottle)
        elif self.pieceArray[self.curPiece]["type"] == "turn":
            self.drift(math.fabs(data[self.myPiece]['angle']))
	    #self.curThrottle = self.speed(self.mph, self.turnSpeed)
	    #self.throttle(self.curThrottle)
	    '''
	    if self.leave_turn(self.curPiece):
	    	if self.approach_turn(self.curPiece):
	    		self.curThrottle = self.speed(self.mph, 5)
	    	        self.throttle(self.curThrottle)
	    	else:
	    	    self.curThrottle = self.speed(self.mph, 5)
	    	    self.throttle(self.curThrottle) 
	    else:
	    	if (self.pieceArray[self.curPiece]['angle'] > 0):
	    	    if (data[self.myPiece]['angle']) < 80: #(self.pieceArray[self.curPiece]['angle']):
			self.curThrottle = self.speed(self.mph, 5)
		        self.throttle(self.curThrottle)
		    else:
		        self.curThrottle = self.speed(self.mph,5)
	                self.throttle(self.curThrottle)
		else:
		    if (data[self.myPiece]['angle']) > 80: #(self.pieceArray[self.curPiece]['angle']):
		        self.curThrottle = self.speed(self.mph, 5)
		        self.throttle(self.curThrottle)
		    else:
		        self.curThrottle = self.speed(self.mph, 5)
		        self.throttle(self.curThrottle)
	    '''



    def on_crash(self, data):
        print("Someone crashed")
	print(self.curPiece)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        finalTime = data['bestLaps'][self.myPiece]['result']['millis'] * 0.001
        print(finalTime)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
	
    def onInit(self, data):
    	print("Recieved initialization")
	for i in range(len(data["race"]["track"]["pieces"])):
	        if 'switch' in data["race"]["track"]["pieces"][i]:
			if 'length' in data["race"]["track"]["pieces"][i]:
				print("straight")
				self.pieceArray.append({ "type": "straight",
							 "length": data["race"]["track"]["pieces"][i]['length'],
							 "switch": True})
			elif ('radius' in data["race"]["track"]["pieces"][i]):
				print("turn")
				self.pieceArray.append({"type": "turn",
							"radius": data["race"]["track"]["pieces"][i]["radius"],
							"angle": data["race"]["track"]["pieces"][i]["angle"],
							"switch":True})
		    
		elif 'length' in data["race"]["track"]["pieces"][i]:
		        print("straight")
			self.pieceArray.append({ 	"type": "straight",
						"length": data["race"]["track"]["pieces"][i]['length']})
		elif ('radius' in data["race"]["track"]["pieces"][i]):
		        print("turn")
			self.pieceArray.append({	"type": "turn",
						"radius": data["race"]["track"]["pieces"][i]["radius"],
						"angle": data["race"]["track"]["pieces"][i]["angle"]})
	for i in range(len(data["race"]["cars"])):
	    if data["race"]["cars"][i]["id"]["name"] == self.name:
	        self.carColor = data["race"]["cars"][i]["id"]["color"]
		print(self.carColor)
		break
	

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.onInit,
	    'turboAvailable': self.turboAvailable,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
